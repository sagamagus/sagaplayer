package com.saga.sagaplayer.domain.usecase

import android.util.Log
import com.saga.sagaplayer.domain.model.Song
import com.saga.sagaplayer.domain.pattern.UseCase
import com.saga.sagaplayer.framework.pattern.LocalSource
import com.saga.sagaplayer.framework.utils.threading.IExecutor

class SaveSong(
    private val local: LocalSource,
    worker: IExecutor,
    handler: IExecutor?
): UseCase<Song,Song>(worker, handler) {

    override fun execute() {
        local.insertSong(input)
        output = local.getSong(input.id)
    }
}