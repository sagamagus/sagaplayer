package com.saga.sagaplayer.domain.model

data class User (
    var id: String = "",
    var email: String = "",
    var password: String = "",
    var username: String = "",
    var names: String = "",
    var last: String = "",
    var bio: String = "",
    var foto: String = ""
)