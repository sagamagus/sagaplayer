package com.saga.sagaplayer.domain.model

data class Song (
    var id: String = "",
    var route: String = "",
    var title: String = "",
    var autor: String = "",
    var cover: String = ""
)