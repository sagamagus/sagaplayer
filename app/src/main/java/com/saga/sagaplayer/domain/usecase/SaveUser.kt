package com.saga.sagaplayer.domain.usecase

import android.util.Log
import com.saga.sagaplayer.domain.model.User
import com.saga.sagaplayer.domain.pattern.UseCase
import com.saga.sagaplayer.framework.pattern.LocalSource
import com.saga.sagaplayer.framework.utils.threading.IExecutor

class SaveUser(
    private val local: LocalSource,
    worker: IExecutor,
    handler: IExecutor?
): UseCase<User,User>(worker, handler) {

    override fun execute() {
        if (local.getUser(input.id).id.equals("")) {
            Log.e("entro","insert")
            local.insertUser(input)
        }else{
            Log.e("entro","update")
            local.updateUser(input)
        }
        output = local.getUser(input.id)
    }
}