package com.saga.sagaplayer.domain.usecase

import android.util.Log
import com.saga.sagaplayer.domain.model.Song
import com.saga.sagaplayer.domain.pattern.UseCase
import com.saga.sagaplayer.framework.pattern.LocalSource
import com.saga.sagaplayer.framework.utils.threading.IExecutor

class GetSongs(
    private val local: LocalSource,
    worker: IExecutor,
    handler: IExecutor?
): UseCase<Unit,List<Song>>(worker, handler) {

    override fun execute() {
        output = local.getSongs()
    }
}