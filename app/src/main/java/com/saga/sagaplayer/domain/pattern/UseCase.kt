package com.saga.sagaplayer.domain.pattern

import com.saga.sagaplayer.framework.utils.threading.IExecutor

abstract class UseCase<I: Any, O: Any>(private val worker: IExecutor,
                                       private val handler: IExecutor?) {

    var callback: Callback<O>? = null

    lateinit var input: I
    lateinit var output: O

    fun run(input : I? = null, onSuccess: ((O)->Unit)? = null) {
        worker.execute(Runnable {
            input?.let { this.input = it }
            execute()
            if (handler == null) {
                callback?.onFinish(output)
                onSuccess?.invoke(output)
            } else {
                handler.execute(Runnable {
                    callback?.onFinish(output)
                    onSuccess?.invoke(output)
                })
            }
        })
    }

    protected abstract fun execute()

    interface Callback<O> {
        fun onFinish(output: O)
    }

}