package com.saga.sagaplayer.framework.utils.threading

import java.util.concurrent.ExecutorService

class XExecutor(private val service: ExecutorService): IExecutor {

    override fun execute(command: Runnable) = service.execute(command)

}