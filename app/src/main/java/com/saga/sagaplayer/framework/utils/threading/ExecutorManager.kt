package com.saga.sagaplayer.framework.utils.threading

import java.util.concurrent.Executors

class ExecutorManager() {

    var main: IExecutor? = null
    val disk: XExecutor = XExecutor(Executors.newSingleThreadExecutor())
    val network: XExecutor = XExecutor(Executors.newFixedThreadPool(POOL_SIZE_NETWORK))
    val computing: XExecutor = XExecutor(Executors.newFixedThreadPool(POOL_SIZE_COMPUTING))

    companion object {

        private const val POOL_SIZE_NETWORK = 3
        private const val POOL_SIZE_COMPUTING = 3

        private var INSTANCE: ExecutorManager? = null

        fun getInstance(): ExecutorManager {
            synchronized(ExecutorManager::class) {
                if (INSTANCE == null) {
                    synchronized(ExecutorManager::class) {
                        INSTANCE =
                            ExecutorManager()
                    }
                }
            }

            return INSTANCE!!
        }

        fun setMainExecutor(executor: IExecutor) {
            INSTANCE?.main = executor
        }

    }

}