package com.saga.sagaplayer.framework.utils.threading

interface IExecutor {
    fun execute(command: Runnable)
}