package com.saga.sagaplayer.framework.source

import android.net.wifi.p2p.WifiP2pManager
import com.saga.sagaplayer.data.DB.Database
import com.saga.sagaplayer.domain.model.Song
import com.saga.sagaplayer.domain.model.User
import com.saga.sagaplayer.framework.pattern.LocalSource

class OnDeviceSource(private val database: Database): LocalSource {

    override fun getUsers(): List<User>{
        return database.getUsers()
    }

    override fun getUser(id: String): User{
        return database.getUser(id)
    }

    override fun findUsers(first: String, last: String): List<User>{
        return database.findUsers(first, last)
    }

    override fun insertUser(user: User){
        database.insertUser(user)
    }

    override fun updateUser(user: User){
        database.updateUser(user)
    }

    override fun deleteUser(user: User){
        database.deleteUser(user)
    }

    override fun getSongs(): List<Song>{
        return database.getSongs()
    }

    override fun getSong(id: String): Song{
        return database.getSong(id)
    }

    override fun searchSongs(search: String): List<Song>{
        return database.searchSongs(search)
    }

    override fun insertSong(song: Song){
        database.insertSong(song)
    }

    override fun updateSong(song: Song){
        database.updateSong(song)
    }

    override fun deleteSong(song: Song){
        database.deleteSong(song)
    }

}