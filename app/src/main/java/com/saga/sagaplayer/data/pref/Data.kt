package com.saga.sagaplayer.data.pref

import android.app.Activity
import android.content.Context
import androidx.preference.PreferenceManager

class Data {

    companion object {

        val LOGGED_PARCIAL = "logged_parcial"
        val LOGGED_COMPLETE = "logged_complete"

        operator fun set(key: String, value: String?, a: Activity?) {
            val e = PreferenceManager.getDefaultSharedPreferences(a).edit()
            e.putString("profile_$key", value)
            e.commit()
        }

        operator fun get(key: String, a: Activity?): String? {
            val data =
                PreferenceManager.getDefaultSharedPreferences(a).all
            val result = data["profile_$key"]
            return result?.toString() ?: ""
        }

        fun has(key: String, a: Activity?): Boolean {
            val data1 =
                PreferenceManager.getDefaultSharedPreferences(a).all
            return data1.containsKey("profile_$key")
        }

        fun clear(a: Activity?) {
            val e = PreferenceManager.getDefaultSharedPreferences(a).edit()
            val data =
                PreferenceManager.getDefaultSharedPreferences(a).all
            for ((key) in data) {
                e.remove(key)
            }
            e.commit()
        }

        fun setC(
            key: String, value: String?, c: Context?
        ) {
            val e = PreferenceManager.getDefaultSharedPreferences(c).edit()
            e.putString("profile_$key", value)
            e.commit()
        }

    }
}