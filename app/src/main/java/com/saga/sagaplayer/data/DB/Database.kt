package com.saga.sagaplayer.data.DB

import com.saga.sagaplayer.domain.model.Song
import com.saga.sagaplayer.domain.model.User

interface Database {

    fun getUsers(): List<User>

    fun getUser(id: String): User

    fun findUsers(first: String, last: String): List<User>

    fun insertUser(user: User)

    fun updateUser(user: User)

    fun deleteUser(user: User)

    fun getSongs(): List<Song>

    fun getSong(id: String): Song

    fun searchSongs(search: String): List<Song>

    fun insertSong(song: Song)

    fun updateSong(song: Song)

    fun deleteSong(song: Song)
}