package com.saga.sagaplayer.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.saga.sagaplayer.domain.model.Song
import com.saga.sagaplayer.domain.model.User

@Entity(tableName = "song")
data class DBSong (
    @ColumnInfo(name= "_id") @PrimaryKey val uid: String,
    @ColumnInfo(name= "route") val route: String,
    @ColumnInfo(name= "title") val title: String,
    @ColumnInfo(name= "autor") val autor: String,
    @ColumnInfo(name= "cover") val cover: String
){
    constructor(song: Song):
            this(song.id,song.route, song.title, song.autor, song.cover)

    fun toModel() =
        Song(uid,route, title, autor, cover)
}