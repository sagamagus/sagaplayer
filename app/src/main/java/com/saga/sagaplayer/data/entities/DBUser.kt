package com.saga.sagaplayer.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.saga.sagaplayer.domain.model.User

@Entity(tableName = "user")
data class DBUser (
    @ColumnInfo(name= "_id") @PrimaryKey val uid: String,
    @ColumnInfo(name= "email") val email: String,
    @ColumnInfo(name= "password") val password: String,
    @ColumnInfo(name= "username") val username: String,
    @ColumnInfo(name= "names") val names: String,
    @ColumnInfo(name= "last_name") val last: String,
    @ColumnInfo(name= "biography") val bio: String,
    @ColumnInfo(name= "foto") val foto: String
){
    constructor(user: User):
            this(
                user.id,user.email, user.password, user.username,
                user.names, user.last, user.bio, user.foto
            )

    fun toModel() =
        User(uid,email, password, username, names, last, bio, foto)
}