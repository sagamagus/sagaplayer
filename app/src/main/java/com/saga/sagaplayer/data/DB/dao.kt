package com.saga.sagaplayer.data.DB

import androidx.room.*
import com.saga.sagaplayer.data.entities.DBSong
import com.saga.sagaplayer.data.entities.DBUser

@Dao
interface dao {

    @Query("SELECT * FROM user")
    fun getAllUsers(): List<DBUser>

    @Query("SELECT * FROM user WHERE _id = :userId")
    fun getUserById(userId: String): DBUser?

    @Query("SELECT * FROM user WHERE names LIKE :first AND " +
            "last_name LIKE :last")
    fun findUserByName(first: String, last: String): List<DBUser>

    @Update
    fun updateUser(user: DBUser)

    @Insert
    fun insertUser(user: DBUser)

    @Delete
    fun deleteUser(user: DBUser)

    @Query("SELECT * FROM song")
    fun getAllSongs(): List<DBSong>

    @Query("SELECT * FROM song WHERE _id = :userId")
    fun getSongById(userId: String): DBSong

    @Query("SELECT * FROM song WHERE title LIKE :search OR " +
            "autor LIKE :search")
    fun findSongByName(search: String): List<DBSong>

    @Update
    fun updateSong(user: DBSong)

    @Insert
    fun insertSong(user: DBSong)

    @Delete
    fun deleteSong(user: DBSong)
}