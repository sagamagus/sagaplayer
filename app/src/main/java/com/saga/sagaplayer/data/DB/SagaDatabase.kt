package com.saga.sagaplayer.data.DB

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.saga.sagaplayer.data.entities.DBSong
import com.saga.sagaplayer.data.entities.DBUser
import com.saga.sagaplayer.domain.model.Song
import com.saga.sagaplayer.domain.model.User
import java.net.UnknownServiceException
import com.saga.sagaplayer.data.DB.Database as DB

@Database(entities = [DBUser::class, DBSong::class], version = 1, exportSchema = false)
abstract class SagaDatabase: RoomDatabase(), DB {
    abstract fun dao(): dao

    companion object {

        private var INSTANCE: SagaDatabase? = null

        fun getInstance(context: Context): SagaDatabase {
            synchronized(SagaDatabase::class) {
                if (INSTANCE == null) {
                    synchronized(SagaDatabase::class) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            SagaDatabase::class.java, "app.data"
                        )
                            .build()
                    }
                }
            }

            return INSTANCE!!
        }

    }

        override fun getUsers(): List<User>{
        return dao().getAllUsers().map { it.toModel() }
    }

    override fun getUser(id: String): User{
        val userDB = dao().getUserById(id)
        var user: User
        if (userDB==null){
            user = User()
        }else{
            user = userDB.toModel()
        }
        return user
    }

    override fun findUsers(first: String, last: String): List<User>{
        return dao().findUserByName(first, last).map { it.toModel() }
    }

    override fun insertUser(user: User){
        dao().insertUser(DBUser(user))
    }

    override fun updateUser(user: User){
        dao().updateUser(DBUser(user))
    }

    override fun deleteUser(user: User){
        dao().deleteUser(DBUser(user))
    }

    override fun getSongs(): List<Song>{
        return dao().getAllSongs().map { it.toModel() }
    }

    override fun getSong(id: String): Song{
        return dao().getSongById(id).toModel()
    }

    override fun searchSongs(search: String): List<Song>{
        return dao().findSongByName(search).map { it.toModel() }
    }

    override fun insertSong(song: Song){
        dao().insertSong(DBSong(song))
    }

    override fun updateSong(song: Song){
        dao().updateSong(DBSong(song))
    }

    override fun deleteSong(song: Song){
        dao().deleteSong(DBSong(song))
    }
}