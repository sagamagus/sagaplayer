package com.saga.sagaplayer.presentation.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.button.MaterialButton
import com.saga.sagaplayer.R
import com.saga.sagaplayer.domain.model.Song
import java.io.File
import java.lang.ref.WeakReference

class SongAdapter(private val context: Context):
    RecyclerView.Adapter<SongAdapter.ViewHolder>() {

    var listener: OnItemActionListener? = null
    var data: List<Song> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_song, parent, false)
        return ViewHolder(
            view
        )
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]

        val title = item.title + " - " + item.autor
        holder.titleView.text = title

        val directory = context.getDir("covers", Context.MODE_PRIVATE)
        val file = File(directory, item.cover)
        Glide.with(context).load(file).into(holder.iconView)
        holder.itemView.setOnClickListener { listener?.onItemSelected(item) }

    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val titleView: AppCompatTextView
                by lazy { view.findViewById<AppCompatTextView>(R.id.tv_song_title_item) }

        val iconView: AppCompatImageView
                by lazy { view.findViewById<AppCompatImageView>(R.id.iv_song_icon_item) }

    }

    interface OnItemActionListener{
        fun onItemSelected(item: Song)
    }

}