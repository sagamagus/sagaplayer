package com.saga.sagaplayer.presentation.ui.home

import android.net.Uri
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.saga.sagaplayer.domain.model.Song
import com.saga.sagaplayer.presentation.pattern.IPresenter
import com.saga.sagaplayer.presentation.pattern.IView

interface HomeContract {

    interface View: IView<Presenter> {

        val db: FirebaseFirestore
        val storage: FirebaseStorage

        fun prepareViews()
        fun loadList()
        fun loadGallery()
        fun showLoading()
        fun hideLoading()
        fun showMessage(message: String)
    }

    interface Presenter: IPresenter {
        override var view: View

        val songs: ArrayList<Song>

    }
}