package com.saga.sagaplayer.presentation.pattern

interface IView<out T: IPresenter> {
    val presenter: T
}