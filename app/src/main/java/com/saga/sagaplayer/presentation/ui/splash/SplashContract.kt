package com.saga.sagaplayer.presentation.ui.splash

import android.graphics.Bitmap
import com.google.firebase.auth.FirebaseAuth
import com.saga.sagaplayer.presentation.pattern.IPresenter
import com.saga.sagaplayer.presentation.pattern.IView

interface SplashContract {

    interface View: IView<Presenter> {

        var mAuth: FirebaseAuth

        fun goLogin()
        fun goHome()
    }

    interface Presenter: IPresenter {
        override var view: View

        fun checkSession()
    }

}