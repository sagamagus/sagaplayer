package com.saga.sagaplayer.presentation.ui.register

import com.google.firebase.auth.FirebaseAuth
import com.saga.sagaplayer.domain.model.User
import com.saga.sagaplayer.presentation.pattern.IPresenter
import com.saga.sagaplayer.presentation.pattern.IView

interface RegisterContract {

    interface View: IView<Presenter> {

        var mAuth: FirebaseAuth

        fun prepareViews()
        fun goLogin()
        fun goHome()
        fun showMessage(message: String)
    }

    interface Presenter: IPresenter {
        override var view: View

        fun register(user: User)
    }

}