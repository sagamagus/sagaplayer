package com.saga.sagaplayer.presentation.ui.login

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.google.firebase.auth.FirebaseAuth
import com.saga.sagaplayer.R
import com.saga.sagaplayer.data.DB.SagaDatabase
import com.saga.sagaplayer.framework.source.OnDeviceSource
import com.saga.sagaplayer.presentation.ui.home.HomeActivity
import com.saga.sagaplayer.presentation.ui.register.RegisterActivity

class LoginActivity : AppCompatActivity(), LoginContract.View {

    override val presenter: LoginContract.Presenter by lazy{
        val local = OnDeviceSource(SagaDatabase.getInstance(this))
        LoginPresenter(this, this, local)
    }

    override lateinit var mAuth: FirebaseAuth
    lateinit var edtEmail: EditText
    lateinit var edtPass: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mAuth = FirebaseAuth.getInstance()
        presenter.start()
    }

    override fun prepareViews() {
        edtEmail = findViewById(R.id.edt_mail)
        edtPass = findViewById(R.id.edt_pass)
        findViewById<Button>(R.id.btn_register)?.setOnClickListener {
            goRegister()
        }
        findViewById<Button>(R.id.btn_login)?.setOnClickListener {
            presenter.logIn(edtEmail.text.toString(), edtPass.text.toString())
        }
    }

    override fun goHome() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun goRegister() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun showMessage(message: String) {
        AlertDialog.Builder(this, R.style.ThemeOverlay_MaterialComponents_Dialog)
            .setTitle("Aviso")
            .setMessage(message)
            .setNegativeButton("Aceptar",null)
            .show()
    }
}