package com.saga.sagaplayer.presentation.ui.track

import android.app.Activity
import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import android.util.Log
import android.widget.Toast
import androidx.core.net.toUri
import com.saga.sagaplayer.R
import com.saga.sagaplayer.data.pref.Data
import com.saga.sagaplayer.domain.model.Song
import com.saga.sagaplayer.domain.usecase.GetSongs
import com.saga.sagaplayer.framework.pattern.LocalSource
import com.saga.sagaplayer.framework.utils.threading.ExecutorManager
import java.io.File

class TrackPresenter(override var view: TrackContract.View, val mActivity: Activity, local: LocalSource): TrackContract.Presenter {

    override val songs: ArrayList<Song> = arrayListOf()
    private val songsBase: ArrayList<Song> = arrayListOf()
    override lateinit var mp: MediaPlayer
    private var index: Int = 0

    private val getSongsUC: GetSongs by lazy {
        val worker = ExecutorManager.getInstance().disk
        val handler = ExecutorManager.getInstance().main
        GetSongs(local, worker, handler)
    }


    override fun start() {
        getSongsUC.run { result ->
            songs.addAll(result)
            songsBase.addAll(result)
        }
        Thread.sleep(1000)
        Log.e("ID3:", view.index)
        val song = songs.first { it.id.equals(view.index) }
        index = songs.indexOf(song)
        val directory = mActivity.getDir("songs", Context.MODE_PRIVATE)
        val file = File(directory, song.route)
        Log.e("entro","music")
        mp = MediaPlayer.create(mActivity, file.toUri())
        mp.start()
        view.prepareViews()
        view.showSong(song)


    }

    override fun handlePlay() {
        if (mp.isPlaying) {
            // Stop
            mp.pause()
            view.playButton.setBackgroundResource(R.drawable.play)

        } else {
            // Start
            mp.start()
            view.playButton.setBackgroundResource(R.drawable.pause)
        }
    }

    override fun handleNext() {
        if (index<songs.size-1) {
            index++
            val song = songs.get(index)
            playSong(song)
        }
    }

    override fun handlePrev() {
        if (index>0) {
            index--
            val song = songs.get(index)
            playSong(song)
        }
    }

    override fun handleRepeat() {
        mp.isLooping = !mp.isLooping
    }

    override fun handleShuffle() {
        if (Data.has("shuffle", mActivity)){
            if (Data.get("shuffle",mActivity).equals("true")){
                songs.clear()
                songs.addAll(songsBase)
                Data.set("shuffle","false",mActivity)

            }else{
                songs.shuffle()
                Data.set("shuffle","true",mActivity)
            }
        }else {
            songs.shuffle()
            Data.set("shuffle","true",mActivity)
        }
    }

    fun playSong(song: Song){
        val directory = mActivity.getDir("songs", Context.MODE_PRIVATE)
        val file = File(directory, song.route)
        mp.reset()
        mp.setDataSource(mActivity, file.toUri())
        mp.prepare()
        mp.start()
        view.showSong(song)
    }
}