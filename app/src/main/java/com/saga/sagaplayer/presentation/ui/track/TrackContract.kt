package com.saga.sagaplayer.presentation.ui.track

import android.media.MediaPlayer
import android.widget.ImageView
import com.saga.sagaplayer.domain.model.Song
import com.saga.sagaplayer.presentation.pattern.IPresenter
import com.saga.sagaplayer.presentation.pattern.IView

interface TrackContract {

    interface View: IView<Presenter> {

        var index: String
        var playButton: ImageView

        fun prepareViews()
        fun showLoading()
        fun hideLoading()
        fun showSong(song: Song)
        fun showMessage(message: String)
    }

    interface Presenter: IPresenter {
        override var view: View

        val songs: ArrayList<Song>
        var mp: MediaPlayer

        fun handleNext()
        fun handlePrev()
        fun handlePlay()
        fun handleShuffle()
        fun handleRepeat()
    }

}