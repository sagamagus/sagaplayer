package com.saga.sagaplayer.presentation.ui.splash

import android.content.Intent
import java.util.*

class SplashPresenter(override var view: SplashContract.View): SplashContract.Presenter {

    override fun start() {
        val timerThread: Thread = object : Thread() {
            override fun run() {
                try {
                    sleep(2000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                } finally {
                    checkSession()
                }
            }
        }

        timerThread.start()
    }

    override fun checkSession() {
        if (view.mAuth.currentUser!=null){
            view.goHome()
        }else{
            view.goLogin()
        }
    }
}