package com.saga.sagaplayer.presentation.ui.register

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.google.firebase.auth.FirebaseAuth
import com.saga.sagaplayer.R
import com.saga.sagaplayer.data.DB.SagaDatabase
import com.saga.sagaplayer.domain.model.User
import com.saga.sagaplayer.framework.source.OnDeviceSource
import com.saga.sagaplayer.presentation.ui.home.HomeActivity
import com.saga.sagaplayer.presentation.ui.login.LoginActivity
import java.lang.StringBuilder

class RegisterActivity : AppCompatActivity(), RegisterContract.View {

    override val presenter: RegisterContract.Presenter by lazy {
        val local = OnDeviceSource(SagaDatabase.getInstance(this))
        RegisterPresenter(this, this, local)
    }

    override lateinit var mAuth: FirebaseAuth
    lateinit var edtEmail: EditText
    lateinit var edtPass: EditText
    lateinit var edtPass2: EditText
    lateinit var edtUser: EditText
    lateinit var edtName: EditText
    lateinit var edtLast: EditText
    lateinit var edtBio: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        mAuth = FirebaseAuth.getInstance()
        presenter.start()
    }

    override fun prepareViews() {
        edtEmail = findViewById(R.id.edt_mail)
        edtPass = findViewById(R.id.edt_pass)
        edtPass2 = findViewById(R.id.edt_pass2)
        edtUser = findViewById(R.id.edt_user)
        edtName = findViewById(R.id.edt_name)
        edtLast = findViewById(R.id.edt_last)
        edtBio = findViewById(R.id.edt_bio)

        findViewById<Button>(R.id.btn_login)?.setOnClickListener {
            goLogin()
        }
        findViewById<Button>(R.id.btn_register)?.setOnClickListener {
            makeUser()
        }
    }

    private fun makeUser(){
        var errors = StringBuilder()
        if (edtEmail.text.toString().length<5) errors.append(R.string.error_mail)
        if (edtPass.text.toString().length<8) errors.append(R.string.error_password)
        if (!edtPass.text.toString().equals(edtPass2.text.toString())) errors.append(R.string.error_passwords)
        if (errors.isEmpty()) {
            val user = User(
                "", edtEmail.text.toString(), edtPass.text.toString(), edtUser.text.toString(),
                edtName.text.toString(), edtLast.text.toString(), edtBio.text.toString(), ""
            )
            presenter.register(user)
        }else{
            showMessage(errors.toString())
        }
    }

    override fun goHome() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun goLogin() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun showMessage(message: String) {
        AlertDialog.Builder(this, R.style.ThemeOverlay_MaterialComponents_Dialog)
            .setTitle("Aviso")
            .setMessage(message)
            .setNegativeButton("Aceptar",null)
            .show()
    }

}