package com.saga.sagaplayer.presentation.ui.home

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.appbar.AppBarLayout
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.saga.sagaplayer.R
import com.saga.sagaplayer.data.DB.SagaDatabase
import com.saga.sagaplayer.domain.model.Song
import com.saga.sagaplayer.framework.source.OnDeviceSource
import com.saga.sagaplayer.presentation.adapters.PhotoAdapter
import com.saga.sagaplayer.presentation.adapters.SongAdapter
import com.saga.sagaplayer.presentation.ui.track.TrackActivity
import java.io.File

class HomeActivity : AppCompatActivity(), HomeContract.View, SongAdapter.OnItemActionListener,
    PhotoAdapter.OnItemActionListener{

    override val db = Firebase.firestore
    override val storage = Firebase.storage
    lateinit var recycler: RecyclerView
    lateinit var btnTracks: TextView
    lateinit var btnCovers: TextView
    lateinit var imageView: ImageView
    lateinit var zoomFragment: View

    override val presenter: HomeContract.Presenter by lazy{
        val local = OnDeviceSource(SagaDatabase.getInstance(this))
        HomePresenter(this, this, local)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        presenter.start()
    }

    override fun prepareViews() {
        recycler = findViewById(R.id.rv_content)
        btnTracks = findViewById(R.id.btn_tracks)
        btnCovers = findViewById(R.id.btn_covers)
        btnTracks.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                btnTracks.setBackgroundColor(getColor(R.color.purple_700))
                btnCovers.setBackgroundColor(getColor(R.color.purple_500))
            } else {
                btnTracks.setBackgroundColor(resources.getColor(R.color.purple_700))
                btnCovers.setBackgroundColor(resources.getColor(R.color.purple_500))
            }
            recycler.layoutManager = LinearLayoutManager(this)
            loadList()
        }
        btnCovers.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                btnCovers.setBackgroundColor(getColor(R.color.purple_700))
                btnTracks.setBackgroundColor(getColor(R.color.purple_500))
            } else {
                btnCovers.setBackgroundColor(resources.getColor(R.color.purple_700))
                btnTracks.setBackgroundColor(resources.getColor(R.color.purple_500))
            }
            recycler.layoutManager = GridLayoutManager(this, 3)
            loadGallery()
        }

        imageView = findViewById(R.id.iv_zoom_display)
        zoomFragment = findViewById(R.id.frame_zoom)
        findViewById<ImageView>(R.id.btn_close).setOnClickListener {
            hideZoom()
        }
    }

    override fun loadList() {
        hideLoading()
        recycler.let { recycler ->
            recycler.adapter =
                SongAdapter(
                    recycler.context
                )
            (recycler.adapter as? SongAdapter)?.let { adapter ->
                adapter.data = presenter.songs
                adapter.listener = this
                adapter.notifyDataSetChanged()
            }
        }
    }

    override fun onItemSelected(item: Song){
        val intent = Intent(this, TrackActivity::class.java)
        intent.putExtra("id", item.id)
        Log.e("ID:", item.id)
        startActivity(intent)

    }

    override fun onPhotoSelected(item: Song){
        val directory = getDir("covers", Context.MODE_PRIVATE)
        val file = File(directory, item.cover)
        Glide.with(this).load(file).into(imageView)
        zoomFragment.visibility = View.VISIBLE
    }

    private fun hideZoom(){
        zoomFragment.visibility = View.GONE
    }

    override fun loadGallery(){
        hideLoading()
        recycler.let { recycler ->
            recycler.adapter =
                PhotoAdapter(
                    recycler.context
                )
            (recycler.adapter as? PhotoAdapter)?.let { adapter ->
                adapter.data = presenter.songs
                adapter.listener = this
                adapter.notifyDataSetChanged()
            }
        }
    }

    override fun showMessage(message: String) {
        AlertDialog.Builder(this, R.style.ThemeOverlay_MaterialComponents_Dialog)
            .setTitle("Aviso")
            .setMessage(message)
            .setNegativeButton("Aceptar",null)
            .show()
    }

    override fun showLoading() {
        findViewById<View>(R.id.cl_processing_container)?.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        findViewById<View>(R.id.cl_processing_container)?.visibility = View.GONE
    }
}