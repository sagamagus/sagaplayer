package com.saga.sagaplayer.presentation.ui.register

import android.app.Activity
import android.util.Log
import com.saga.sagaplayer.domain.model.User
import com.saga.sagaplayer.domain.usecase.SaveUser
import com.saga.sagaplayer.framework.pattern.LocalSource
import com.saga.sagaplayer.framework.utils.threading.ExecutorManager

class RegisterPresenter(override var view: RegisterContract.View, var mActivity: Activity, local: LocalSource):RegisterContract.Presenter {

    private val saveUserUC: SaveUser by lazy {
        val worker = ExecutorManager.getInstance().disk
        val handler = ExecutorManager.getInstance().computing
        SaveUser(local, worker, handler)
    }

    override fun start() {
        view.prepareViews()
    }

    override fun register(user: User) {
        view.mAuth.createUserWithEmailAndPassword(user.email, user.password)
            .addOnCompleteListener(mActivity) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("paso", "createUserWithEmail:success")
                    user.id = view.mAuth.currentUser!!.uid
                    saveUser(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("no paso", "createUserWithEmail:failure", task.exception)
                    view.showMessage(task.exception.toString())
                }

                // ...
            }
    }

    private fun saveUser(user: User){
        saveUserUC.run(user) { result->
            Log.e("id:",result.id)
            view.goHome()
        }
    }
}