package com.saga.sagaplayer.presentation.ui.home

import android.app.Activity
import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import android.util.Log
import androidx.core.net.toUri
import com.saga.sagaplayer.data.pref.Data
import com.saga.sagaplayer.domain.model.Song
import com.saga.sagaplayer.domain.usecase.GetSongs
import com.saga.sagaplayer.domain.usecase.SaveSong
import com.saga.sagaplayer.framework.pattern.LocalSource
import com.saga.sagaplayer.framework.utils.threading.ExecutorManager
import java.io.*

class HomePresenter(override var view: HomeContract.View, val mActivity: Activity, local: LocalSource): HomeContract.Presenter {

    override val songs: ArrayList<Song> = arrayListOf()


    private val saveSongUC: SaveSong by lazy {
        val worker = ExecutorManager.getInstance().disk
        val handler = ExecutorManager.getInstance().computing
        SaveSong(local, worker, handler)
    }

    private val getSongsUC: GetSongs by lazy {
        val worker = ExecutorManager.getInstance().disk
        val handler = ExecutorManager.getInstance().computing
        GetSongs(local, worker, handler)
    }

    override fun start() {
        view.prepareViews()
        if (Data.has("music_dowloaded", mActivity)){
            if (Data.get("music_dowloaded",mActivity).equals("true")){
                loadSongs()
            }else{
                synDB()
            }
        }else{
            synDB()
        }
    }

    private fun loadSongs(){
        getSongsUC.run { result ->

            songs.addAll(result)
            /*for (song in result){
                songs.add(song.route)
                covers.add(song.cover)
                val directory = mActivity.getDir("songs", Context.MODE_PRIVATE)
                val file = File(directory, song.route)
                songsToPlay.add(file.toUri())
            }*/


        }
        view.loadList()
    }

    private fun synDB() {
        Log.e("entro","sync")
        view.db.collection("songs")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    val song = Song()
                    song.id = document.id
                    document.getString("route")?.let { song.route=  it }
                    document.getString("cover")?.let { song.cover=  it }
                    document.getString("titulo")?.let { song.title = it  }
                    document.getString("autor")?.let { song.autor= it }
                    saveSongUC.run(song)
                    songs.add(song)
                }
                downloadCovers()
            }
            .addOnFailureListener { exception ->
                exception.message?.let { view.showMessage(it) }
            }


    }

    private fun downloadCovers() {
        Log.e("entro","download")
        val storageRef = view.storage.reference
        var downloaded = 0
        for (song in songs){
            val pathReference = storageRef.child(song.cover)
            val directory = mActivity.getDir("covers", Context.MODE_PRIVATE)
            val file = File(directory, song.cover)
            pathReference.getFile(file).addOnSuccessListener {
                // Local temp file has been created
                downloaded++
                Log.e("descargado:", downloaded.toString())
                if (downloaded==songs.size){
                    downloadMusic()
                }
            }.addOnFailureListener {
                // Handle any errors
            }
        }
    }

    private fun downloadMusic() {
        Log.e("entro","download")
        val storageRef = view.storage.reference
        var downloaded = 0
        for (song in songs){
            val pathReference = storageRef.child(song.route)
            val directory = mActivity.getDir("songs", Context.MODE_PRIVATE)
            val file = File(directory, song.route)
            pathReference.getFile(file).addOnSuccessListener {
                // Local temp file has been created

                Log.e("se descargó",file.toUri().toString())
                downloaded++
                Log.e("descargado:", downloaded.toString())
                if (downloaded==songs.size){
                    Data.set("music_dowloaded","true",mActivity)
                    view.loadList()
                }
            }.addOnFailureListener {
                // Handle any errors
            }
        }
    }


}