package com.saga.sagaplayer.presentation.ui.login

import android.app.Activity
import android.util.Log
import com.saga.sagaplayer.domain.model.User
import com.saga.sagaplayer.domain.usecase.SaveUser
import com.saga.sagaplayer.framework.pattern.LocalSource
import com.saga.sagaplayer.framework.utils.threading.ExecutorManager

class LoginPresenter(override var view: LoginContract.View, val mActivity: Activity, local:LocalSource):LoginContract.Presenter {

    private val saveUserUC: SaveUser by lazy {
        val worker = ExecutorManager.getInstance().disk
        val handler = ExecutorManager.getInstance().computing
        SaveUser(local, worker, handler)
    }

    override fun start() {
        view.prepareViews()
    }

    override fun logIn(email: String, pass: String) {
        view.mAuth.signInWithEmailAndPassword(email, pass)
            .addOnCompleteListener(mActivity) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("paso", "signInWithEmail:success")
                    //view.goHome()
                    saveUser(email,pass)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("no paso", "signInWithEmail:failure", task.exception)
                    view.showMessage(task.exception.toString())
                }

            }
    }

    private fun saveUser(email: String, pass: String ){
        val user = User(view.mAuth.currentUser!!.uid,email,pass,"","","","","")
        saveUserUC.run(user) { result->
            Log.e("id:",result.id)
            view.goHome()
        }
    }
}