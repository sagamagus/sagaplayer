package com.saga.sagaplayer.presentation.pattern

interface IPresenter {

    val view : IView<IPresenter>

    fun start()

}