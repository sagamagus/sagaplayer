package com.saga.sagaplayer.presentation.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.saga.sagaplayer.R
import com.saga.sagaplayer.presentation.ui.home.HomeActivity
import com.saga.sagaplayer.presentation.ui.login.LoginActivity

class SplashActivity : AppCompatActivity(), SplashContract.View {

    override lateinit var mAuth: FirebaseAuth

    override val presenter: SplashContract.Presenter by lazy{

        SplashPresenter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        mAuth = FirebaseAuth.getInstance()
        presenter.start()
    }

    override fun goHome() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun goLogin() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
}