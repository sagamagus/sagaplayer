package com.saga.sagaplayer.presentation.ui.login

import com.google.firebase.auth.FirebaseAuth
import com.saga.sagaplayer.presentation.pattern.IPresenter
import com.saga.sagaplayer.presentation.pattern.IView

interface LoginContract {
    interface View: IView<Presenter> {

        var mAuth: FirebaseAuth

        fun prepareViews()
        fun goRegister()
        fun goHome()
        fun showMessage(message: String)
    }

    interface Presenter: IPresenter {
        override var view: View

        fun logIn(email: String, pass: String)
    }
}