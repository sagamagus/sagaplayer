package com.saga.sagaplayer.presentation.ui.track

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.saga.sagaplayer.R
import com.saga.sagaplayer.data.DB.SagaDatabase
import com.saga.sagaplayer.data.pref.Data
import com.saga.sagaplayer.domain.model.Song
import com.saga.sagaplayer.framework.source.OnDeviceSource
import java.io.File

class TrackActivity : AppCompatActivity(), TrackContract.View {

    override lateinit var index: String
    override lateinit var playButton: ImageView
    lateinit var coverView: ImageView
    lateinit var titleView: TextView
    lateinit var positionBar: SeekBar
    lateinit var elapsed: TextView
    lateinit var remaining: TextView
    var totalTime: Int = 0
    private var loop: Boolean = false

    override val presenter: TrackContract.Presenter by lazy{
        val local = OnDeviceSource(SagaDatabase.getInstance(this))
        TrackPresenter(this, this, local)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track)
        intent.getStringExtra("id")?.let {
            Log.e("ID2:", it)
            index = it
        }
        presenter.start()
    }

    override fun prepareViews() {
        coverView = findViewById(R.id.img_cover)
        titleView = findViewById(R.id.txt_title)
        positionBar = findViewById(R.id.positionBar)
        elapsed = findViewById(R.id.txt_elapsed)
        remaining = findViewById(R.id.txt_remaining)
        playButton = findViewById(R.id.btn_play)
        playButton.setOnClickListener { presenter.handlePlay() }
        findViewById<ImageView>(R.id.btn_next).setOnClickListener { presenter.handleNext() }
        findViewById<ImageView>(R.id.btn_back).setOnClickListener { presenter.handlePrev() }
        findViewById<ImageView>(R.id.btn_shuffle).setOnClickListener {
            if (Data.has("shuffle", this)){
                if (Data.get("shuffle",this).equals("true")){
                    Toast.makeText(this, "Reproducción Normal", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this, "Reproducción Aleatoria", Toast.LENGTH_SHORT).show()
                }
            }else {
                Toast.makeText(this, "Reproducción Aleatoria", Toast.LENGTH_SHORT).show()
            }
            presenter.handleShuffle()
        }
        findViewById<ImageView>(R.id.btn_loop).setOnClickListener {
            loop = !loop
            if (loop){
                Toast.makeText(this, "Repetir", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this, "No Repetir", Toast.LENGTH_SHORT).show()
            }
            presenter.handleRepeat()
        }
        positionBar.setOnSeekBarChangeListener(
            object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    if (fromUser) {
                        presenter.mp.seekTo(progress)
                    }
                }
                override fun onStartTrackingTouch(p0: SeekBar?) {
                }
                override fun onStopTrackingTouch(p0: SeekBar?) {
                }
            }
        )
        Thread(Runnable {
            while (presenter.mp != null) {
                try {
                    var msg = Message()
                    msg.what = presenter.mp.currentPosition
                    handler.sendMessage(msg)
                    Thread.sleep(1000)
                } catch (e: InterruptedException) {
                }
            }
        }).start()
    }

    @SuppressLint("HandlerLeak")
    var handler = object : Handler() {
        override fun handleMessage(msg: Message) {
            var currentPosition = msg.what

            // Update positionBar
            positionBar.progress = currentPosition

            // Update Labels
            var elapsedTime = createTimeLabel(currentPosition)
            elapsed.text = elapsedTime

            var remainingTime = createTimeLabel(totalTime - currentPosition)
            remaining.text = "-$remainingTime"
        }
    }

    fun createTimeLabel(time: Int): String {
        var timeLabel = ""
        var min = time / 1000 / 60
        var sec = time / 1000 % 60

        timeLabel = "$min:"
        if (sec < 10) timeLabel += "0"
        timeLabel += sec

        return timeLabel
    }

    override fun showSong(song: Song){
        val directory = getDir("covers", Context.MODE_PRIVATE)
        val file = File(directory, song.cover)
        Glide.with(this).load(file).into(coverView)
        val title = song.title + " - " + song.autor
        titleView.setText(title)
        totalTime = presenter.mp.duration
        positionBar.max = totalTime

    }

    override fun showMessage(message: String) {
        AlertDialog.Builder(this, R.style.ThemeOverlay_MaterialComponents_Dialog)
            .setTitle("Aviso")
            .setMessage(message)
            .setNegativeButton("Aceptar",null)
            .show()
    }

    override fun showLoading() {
        findViewById<View>(R.id.cl_processing_container)?.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        findViewById<View>(R.id.cl_processing_container)?.visibility = View.GONE
    }
}